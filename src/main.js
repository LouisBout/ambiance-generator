import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import { EventBus } from "@/services/event-bus";

Vue.config.productionTip = false;

Vue.prototype.$eventBus = EventBus;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
