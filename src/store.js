import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from 'vuex-persist'
import device from "@/modules/device";

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  modules: { device },
  plugins: [vuexLocal.plugin]
});
