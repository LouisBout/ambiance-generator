import AudioClipSensor from "./AudioClipSensor";
import ActuationSensor from "./ActuationSensor";

export const sensorsMapping = [
  {
    type: 3339,
    nativeSensorId: 0,
    resource: 5522,
    label: 'Audio Clip Sensor',
    component: AudioClipSensor
  },
  {
    type: 3306,
    nativeSensorId: 1,
    resource: 5850,
    label: 'Actuation Sensor',
    component: ActuationSensor
  }
];

export const sensors = {
  AudioClipSensor,
  ActuationSensor
}

export const getSensorComponentByType = type => {
  let idx = sensorsMapping.findIndex(sensor => sensor.type === type);
  let sensor = sensorsMapping[idx];
  return sensor.component;
};

