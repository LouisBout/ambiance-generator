import AudioPadWidget from "./AudioPadWidget";
import { sensorsMapping } from "../sensors";

export const widgetsMapping = [
  {
    type: 1,
    sensors: sensorsMapping,
    label: "Audio pad widget ",
    component: AudioPadWidget
  }
];

export const widgets = {
  AudioPadWidget
};

export const getWidgetByType = type => {
  const idx = widgetsMapping.findIndex(widget =>
    widget.type === type
  );
  const widget = widgetsMapping[idx];
  return widget;
};
