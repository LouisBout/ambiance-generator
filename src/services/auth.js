import { http } from "./api";

export const authenticate = credentials =>
  http.post("/Devices/authenticate", credentials);

export const getFullState = (deviceId, headers) =>
  http.get(`Devices/get-full-state/${deviceId}`, {
    headers
  });
