import mqtt from "async-mqtt";
import store from "@/store";
import { EventBus } from "./event-bus";

const Storage = window.sessionStorage;

const brokerUrl = process.env.VUE_APP_BROKER_URL;

let baseOptions = {
  //  keepalive: 60,
  // reschedulePings: true,
  protocolId: "MQTT",
  protocolVersion: 4,
  reconnectPeriod: 3000,
  connectTimeout: 30 * 1000,
  clean: true,
  clientId: "",
  username: "",
  password: ""
};

const device = store.getters["device/device"] || {};

baseOptions = {
  ...baseOptions,
  clientId: device.devEui,
  username: device.id,
  password: device.apiKey
};

const socket = {};

const setSocketId = socketId => {
  if (Storage) {
    Storage.setItem("socket-id", socketId);
  }
};

const delSocketId = () => {
  if (Storage) {
    Storage.removeItem("socket-id");
  }
};

const getSocketId = () => {
  const socketId = Storage && Storage.getItem("socket-id");
  return socketId;
};

socket.initSocket = async (options = baseOptions) => {
  try {
    let socketId = getSocketId();
    if (socketId && socketId !== null && socket.client) {
      return socket;
    }
    socket.client = await mqtt.connectAsync(brokerUrl, options);
    setSocketId(options.clientId);
    await socket.client.subscribe(`${device.devEui}-in/#`);

    socket.client.on("offline", () => {
      delSocketId();
    });


    socket.client.on("message", (topic, message) => {
      const parts = topic.split("/");
      const payload = {
        topic: null,
        method: null,
        type: null,
        nodeId: null,
        sensorId: null,
        resource: null
      };

      parts.forEach((part, idx) => {
        const payloadIndex = Object.keys(payload)[idx];
        payload[payloadIndex] = part;
      });

      EventBus.$emit("publish", { payload, message });
    });

    return socket;
  } catch (error) {
    throw error;
  }
};

export default socket;
