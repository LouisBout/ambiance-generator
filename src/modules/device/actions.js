import * as auth from "@/services/auth";
import socket from "@/services/mqtt";

export const authenticate = async ({ commit }, credentials) => {
  try {
    const response = await auth.authenticate(credentials);
    commit("authenticationSuccess", response.data);
  } catch (error) {
    commit("authenticationFailed", error);
  }
};

export const logout = ({ commit }) => {
  commit("logout");
};

export const getFullState = async ({ commit, state }) => {
  try {
    const headers = {
      apiKey: state.device.apiKey,
      devEui: state.device.devEui
    };
    const response = await auth.getFullState(state.device.id, headers);
    commit("getFullStateSuccess", response.data);
    commit("configureWidgets");
  } catch (error) {
    commit("getFullStateFailed", error);
  }
};

export const removeWidget = ({ commit }, nativeNodeId) => {
  commit("removeWidget", nativeNodeId);
};

export const removeSensor = ({ commit }, sensor) => {
  commit("removeSensor", sensor);
  commit("configureWidgets");
};

export const publishResourceValue = (
  { state, dispatch },
  { sensor, resource, value }
) => {
  dispatch("publishMQTT", {
    topic: `${state.device.devEui}-in/1/${sensor.type}/${sensor.nativeNodeId}/${sensor.nativeSensorId}/${resource}`,
    payload: value
  });
};

export const setResourceValue = ({ state }, { sensor, resource, value }) => {
  state.widgets[parseInt(sensor.nativeNodeId)][sensor.nativeSensorId].resources[
    resource
  ] = value;
};

export const publishWidget = ({ state, commit, dispatch }, widget) => {
  try {
    if (state.sensors.length > 0) {
      commit("incrementNodeId");
    }
    widget.sensors.forEach(sensor => {
      dispatch("publishMQTT", {
        topic: `${state.device.devEui}-in/0/${sensor.type}/${state.maxNativeNodeId}/${sensor.nativeSensorId}/${sensor.resource}`,
        payload: "notsaved"
      });
    });
  } catch (error) {
    commit("getWidgetFailed", error);
  }
};

export const deleteWidget = (
  { state, commit, dispatch },
  { nativeNodeId, widget }
) => {
  try {
    widget.sensors.forEach(sensor => {
      dispatch("publishMQTT", {
        topic: `${state.device.devEui}-in/3/${sensor.type}/${nativeNodeId}/${sensor.nativeSensorId}/${sensor.resource}`,
        payload: "notsaved"
      });
    });
  } catch (error) {
    commit("getWidgetFailed", error);
  }
};

export const publishMQTT = async ({ commit }, { topic, payload }) => {
  try {
    await socket.client.publish(topic, payload);
    commit("publishSuccess");
  } catch (error) {
    commit("publishFailed", error);
  }
};
