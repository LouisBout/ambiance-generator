import * as actions from './actions';
import * as mutations from './mutations';
import getters from './getters';

export default {
  namespaced: true,
  state: {
    error: null,
    isConnected: false,
    device: {},
    sensors: [],
    widgets: {},
    maxNativeNodeId: 0,
    keyType: null
  },
  getters,
  mutations,
  actions,
};
