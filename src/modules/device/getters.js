export default {
  device: state => state.device,
  getCredentials: state => ({
    apiKey: state.device.apiKey,
    deviceId: state.device.id
  }),
  keyType: state => state.keyType,
  isConnected: state => state.isConnected,
  getSensors: state => state.sensors,
  getWidget: state => (nativeNodeId) => {
    if(state.widgets[nativeNodeId]){
      return state.widgets[nativeNodeId]
    }
  },
  getSensor: state => (nativeNodeId, type) => {
    if (state.widgets[nativeNodeId]){
    const sensor = state.widgets[nativeNodeId].filter(
      sensor => sensor.type === type
    )[0];
    return sensor;
    }
  },
  getWidgets: state => state.widgets
};
