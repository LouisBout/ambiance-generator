export const authenticationSuccess = (state, payload) => {
  state.isConnected = true;
  state.device = payload.device;
  state.keyType = payload.keyType;
};

export const authenticationFailed = (state, error) => {
  state.error = error;
  state.isConnected = false;
};

export const logout = state => {
  state.device = {};
  state.keyType = null;
  state.isConnected = false;
};

export const getFullStateSuccess = (state, payload) => {
  state.sensors = payload.sensors;
};

export const configureWidgets = (state) => {
  const nativeNodeIds = state.sensors.map(
    sensor => parseInt(sensor.nativeNodeId) || 0
  );

  let widgets = {};
  for (let nodeId of new Set(nativeNodeIds)) {
    widgets[nodeId] = 
      state.sensors
        .filter(sensor => parseInt(sensor.nativeNodeId) === nodeId)
        .sort((a, b) => parseInt(a.nativeSensorId) - parseInt(b.nativeSensorId))
    ;
  }

  state.widgets = widgets

  state.maxNativeNodeId = state.sensors.length ? Math.max(...nativeNodeIds) : 0;
}

export const getFullStateFailed = (state, error) => {
  state.error = error;
};

export const getSensorFailed = (state, error) => {
  state.error = error;
};

export const incrementNodeId = state => {
  state.maxNativeNodeId += 1;
};


export const getWidgetFailed = (state, error) => {
  state.error = error;
};

export const publishSuccess = state => {};

export const publishFailed = (state, error) => {
  state.error = error;
};
