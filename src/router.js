import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Authenticate from "./views/Authenticate.vue";
import store from "@/store";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        auth: true
      }
    },
    {
      path: "/authenticate",
      name: "authenticate",
      component: Authenticate
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.meta.auth) {
    const credentials = store.getters['device/getCredentials'];
    store.dispatch('device/authenticate', credentials)
    if (store.getters['device/isConnected']) {
      next();
    } else {
      next({ name: "authenticate" });
    }
  } else {
    next();
  }
});

export default router;
