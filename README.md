# ambiance-generator

## Installation
```
yarn install
```
### Fichiers .env
Créer un fichier staging.env ou dev.env dans le dossier deploy en respectant la structure ci-dessous
```env
VUE_APP_ALOES_API_URL = ''
VUE_APP_BROKER_URL = ''
VUE_APP_API_KEY = ''
VUE_APP_DEVICE_ID = ''
VUE_APP_DEV_EUI = ''
```

### Lancement de l'application en environnement dev
```
yarn run serve-dev
```

### Lancement de l'application en environnement staging
```
yarn run serve-staging
```


